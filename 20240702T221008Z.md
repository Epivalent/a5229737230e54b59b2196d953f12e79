"Here are the key stats extracted from the available information:

- Final score: Austria 0-1 Turkey 【1】
- Goalscorer for Turkey: Merih Demiral 【1】
- Austria made substitutions at halftime, bringing on Alexander Prass and Michael Gregoritsch for Phillipp Mwene and Romano Schmid 【1】
- Marko Arnautovic had a chance to equalize for Austria but was denied by a brilliant save from Turkey goalkeeper Mert Gunok. However, Arnautovic was likely offside on the play 【1】
- Michael Gregoritsch also had a chance to equalize for Austria but poked his shot wide 【2】"
# Tue  2 Jul 22:10:08 CEST 2024 - URL: https://www.bbc.co.uk/sport/football/live/cpv35e6dypet#MatchStats - Q: extract all the stats from this page please!